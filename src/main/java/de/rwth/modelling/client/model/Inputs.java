/* (c) https://github.com/MontiCore/monticore */
package de.rwth.modelling.client.model;

public final class Inputs {

    private double timeIncrement;
    private double currentVelocity;
    private double x;
    private double y;
    private double compass;
    private double currentEngine;
    private double currentSteering;
    private double currentBrakes;
    private int trajectoryLength;
    private double[] trajectoryX;
    private double[] trajectoryY;

    public double getTimeIncrement() {
        return timeIncrement;
    }

    public void setTimeIncrement(double timeIncrement) {
        this.timeIncrement = timeIncrement;
    }

    public double getCurrentVelocity() {
        return currentVelocity;
    }

    public void setCurrentVelocity(double currentVelocity) {
        this.currentVelocity = currentVelocity;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getCompass() {
        return compass;
    }

    public void setCompass(double compass) {
        this.compass = compass;
    }

    public double getCurrentEngine() {
        return currentEngine;
    }

    public void setCurrentEngine(double currentEngine) {
        this.currentEngine = currentEngine;
    }

    public double getCurrentSteering() {
        return currentSteering;
    }

    public void setCurrentSteering(double currentSteering) {
        this.currentSteering = currentSteering;
    }

    public double getCurrentBrakes() {
        return currentBrakes;
    }

    public void setCurrentBrakes(double currentBrakes) {
        this.currentBrakes = currentBrakes;
    }

    public int getTrajectoryLength() {
        return trajectoryLength;
    }

    public void setTrajectoryLength(int trajectoryLength) {
        this.trajectoryLength = trajectoryLength;
    }

    public double[] getTrajectoryX() {
        return trajectoryX;
    }

    public void setTrajectoryX(double[] trajectoryX) {
        this.trajectoryX = trajectoryX;
    }

    public double[] getTrajectoryY() {
        return trajectoryY;
    }

    public void setTrajectoryY(double[] trajectoryY) {
        this.trajectoryY = trajectoryY;
    }
}
