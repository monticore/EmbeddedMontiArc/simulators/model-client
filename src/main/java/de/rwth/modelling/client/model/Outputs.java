/* (c) https://github.com/MontiCore/monticore */
package de.rwth.modelling.client.model;

public final class Outputs {

    private double engine;
    private double steeringAngle;
    private double brakes;

    public double getEngine() {
        return engine;
    }

    public void setEngine(double engine) {
        this.engine = engine;
    }

    public double getSteeringAngle() {
        return steeringAngle;
    }

    public void setSteeringAngle(double steeringAngle) {
        this.steeringAngle = steeringAngle;
    }

    public double getBrakes() {
        return brakes;
    }

    public void setBrakes(double brakes) {
        this.brakes = brakes;
    }
}
