/* (c) https://github.com/MontiCore/monticore */
package de.rwth.modelling.client;

import de.rwth.modelling.Actuation;
import de.rwth.modelling.ModelGrpc;
import de.rwth.modelling.ModelInputs;
import de.rwth.modelling.ModelOutputs;
import de.rwth.modelling.Point;
import de.rwth.modelling.SensorData;
import de.rwth.modelling.Trajectory;
import de.rwth.modelling.client.model.Inputs;
import de.rwth.modelling.client.model.Outputs;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public final class SimpleClient {

    private static final Logger LOGGER = Logger.getLogger(SimpleClient.class);

    private final ManagedChannel channel;
    private final ModelGrpc.ModelBlockingStub blockingStub;

    public SimpleClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build());
    }

    SimpleClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = ModelGrpc.newBlockingStub(channel);
    }

    public Outputs executeModel(Inputs inputs) {
        Outputs result = new Outputs();
        Actuation actuation = Actuation.newBuilder()
                .setEngine(inputs.getCurrentEngine())
                .setSteeringAngle(inputs.getCurrentSteering())
                .setBrakes(inputs.getCurrentBrakes())
                .build();
        SensorData sensorData = SensorData.newBuilder()
                .setVelocity(inputs.getCurrentVelocity())
                .setPosition(Point.newBuilder().setX(inputs.getX()).setY(inputs.getY()))
                .setCompass(inputs.getCompass())
                .build();
        ModelInputs request = ModelInputs.newBuilder()
                .setTimeIncrement(inputs.getTimeIncrement())
                .setActuation(actuation)
                .setSensorData(sensorData)
                .setTrajectory(getTrajectory(inputs))
                .build();
        ModelOutputs response;
        try {
            response = blockingStub.execute(request);
        } catch (StatusRuntimeException e) {
            LOGGER.warn("RPC failed", e);
            return result;
        }
        if (response.hasActuation()) {
            Actuation a = response.getActuation();
            result.setEngine(a.getEngine());
            result.setSteeringAngle(a.getSteeringAngle());
            result.setBrakes(a.getBrakes());
        } else {
            LOGGER.warn("no actuation has been provided");
        }
        return result;
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    private static Trajectory getTrajectory(Inputs inputs) {
        Trajectory.Builder tb = Trajectory.newBuilder();
        double[] xs = inputs.getTrajectoryX();
        double[] ys = inputs.getTrajectoryY();
        for (int i = 0, len = inputs.getTrajectoryLength(); i < len; i++) {
            Point p = Point.newBuilder().setX(xs[i]).setY(ys[i]).build();
            tb.addPoints(p);
        }
        return tb.build();
    }
}
